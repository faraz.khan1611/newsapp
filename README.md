This is a sample News Reader app that is supposed to display news list and the details.
The first page displays news list, when one of the items is clicked, it is supposed to show the detail of the selected news.

The project Multi pane view for tablet screen.

## Screenshots

**News List View**



![Webp.net-resizeimage](/uploads/ee10ce759de073decb8479614bbd3a0b/Webp.net-resizeimage.jpg)

**News Details View**



![Webp.net-resizeimage](/uploads/26810991ecd86ba378e5fff2abd8c7ce/Webp.net-resizeimage.png)

**Browser view**



![Webp.net-resizeimage__1_](/uploads/d144e7408ade7d3db392d34d8a6c539c/Webp.net-resizeimage__1_.png)


## About the project
All the data is coming from the web endpoint.
The response contains a list of news items as well as URLs to the pictures associated with each story.

## Unit test Coverage

![Screen_Shot_2019-06-24_at_5.02.16_PM](/uploads/9b57cd069607c67ed8eeba11afed60db/Screen_Shot_2019-06-24_at_5.02.16_PM.png)