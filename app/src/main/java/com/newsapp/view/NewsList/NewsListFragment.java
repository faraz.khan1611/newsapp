package com.newsapp.view.NewsList;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.app.ListFragment;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.newsapp.ModelLayer.Entity.MediaEntity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import com.newsapp.ModelLayer.Entity.NewsEntity;
import com.newsapp.ModelLayer.Network.Error;
import news.agoda.com.sample.R;

public class NewsListFragment extends ListFragment implements ICallback<List<NewsEntity>> {
    public static final String TAG = NewsListFragment.class.getName();
    private OnNewsItemSelectedListener mActivityCallback;

    private final List<NewsEntity> mNewsItemList = new ArrayList<>();
    private NewsListAdapter mAdapter;
    private NewsListPresenter mPresenter;
    private Button mRetryButton;
    private ProgressBar mProgressBar;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    public interface OnNewsItemSelectedListener {
        void onNewsItemClick(String title, String storyURL, String summary, String imageURL);
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setupRetryButton() {
        mRetryButton = (Button) getActivity().findViewById(R.id.retry_button);
        if (mRetryButton != null) {
            mRetryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRetryButton.setVisibility(View.GONE);
                    loadNewsResource();
                }
            });
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRetryButton();
        mProgressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);
        setListAdapter();
        showProgress();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getFragmentManager().findFragmentById(R.id.news_details_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mActivityCallback = (OnNewsItemSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnNewsItemSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityCallback = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        NewsEntity newsEntity = mNewsItemList.get(position);
        String title = newsEntity.getTitle();
        String storyURL = newsEntity.getArticleUrl();
        String summary = newsEntity.getSummary();
        String imageURL = "";
        if (!newsEntity.getMediaEntity().isEmpty()) {
            MediaEntity mediaEntity = newsEntity.getMediaEntity().get(0);
            imageURL = mediaEntity.getUrl();
        }
        if (mActivityCallback != null) {
            mActivityCallback.onNewsItemClick(title, storyURL, summary, imageURL);
        }
        getListView().setItemChecked(position, true);
        getListView().setSelection(position);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.removeCallback();
        }
        mHandler = null;
    }

    private void showProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.animate();
        }
    }

    private void hideProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    //region initialize
    public void setPresenter(@NotNull NewsListPresenter presenter) {
        this.mPresenter = presenter;
        //No need to load resource here as the news is already loaded as object is retained
        loadNewsResource();
    }

    private void loadNewsResource() {
        showProgress();
        this.mPresenter.setCallback(this);
        this.mPresenter.loadNewsResource();
    }
    //endregion

    //region Adapter Related Methods
    private void setListAdapter() {
        mAdapter = new NewsListAdapter(getActivity(), R.layout.list_item_news, mNewsItemList);
        setListAdapter(mAdapter);
        if (getFragmentManager().findFragmentById(R.id.news_details_fragment) != null) {
            if (getListView().getAdapter() != null) {
                if (getListView().getAdapter().getCount() > 0) {
                    getListView().performItemClick(getListView().getChildAt(0), 0, 0);
                }
            }
        }
    }

    private void updateListAdapter(@NotNull List<NewsEntity> list) {
        mNewsItemList.addAll(list);
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        if (getFragmentManager().findFragmentById(R.id.news_details_fragment) != null) {
            if (getListView().getAdapter().getCount() > 0) {
                getListView().performItemClick(getListView().getChildAt(0), 0, 0);
            }
        }

    }
    //endregion

    //region Presenter ICallback
    @Override
    public void onResult(final List<NewsEntity> data) {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    hideProgress();
                    updateListAdapter(data);
                }
            });
        }
    }

    @Override
    public void onError(final Error error) {
        if (mHandler != null) {
            //Handle error here
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    hideProgress();
                    if (mRetryButton != null) {
                        mRetryButton.setVisibility(View.VISIBLE);
                    }
                    Toast.makeText(getActivity(), "Error found :: " + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    //endregion
}
