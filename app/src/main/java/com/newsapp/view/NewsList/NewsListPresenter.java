package com.newsapp.view.NewsList;

public interface NewsListPresenter {
    void loadNewsResource();

    void setCallback(ICallback callback);

    void removeCallback();
}

