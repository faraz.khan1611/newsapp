package com.newsapp.view.NewsList;
import com.newsapp.ModelLayer.Network.Error;

interface ICallback<T> {
    void onResult(T data);

    void onError(Error error);
}
