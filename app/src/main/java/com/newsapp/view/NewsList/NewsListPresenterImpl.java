package com.newsapp.view.NewsList;

import com.newsapp.ModelLayer.InteractorConsumer;
import com.newsapp.ModelLayer.InteractorLayer;
import com.newsapp.ModelLayer.Network.Error;

import java.util.List;

import com.newsapp.ModelLayer.Entity.NewsEntity;

public class NewsListPresenterImpl implements NewsListPresenter, InteractorConsumer {
    private final InteractorLayer mInteractorLayer;
    private ICallback<List<NewsEntity>> mCallback;

    public NewsListPresenterImpl(InteractorLayer interactorLayer) {
        this.mInteractorLayer = interactorLayer;
    }

    @Override
    public void loadNewsResource() {
        this.mInteractorLayer.loadNewsResource(this);
    }

    @SuppressWarnings("unchecked")
    public void setCallback(ICallback callback) {
        this.mCallback = callback;
    }

    @Override
    public void removeCallback() {
        this.mCallback = null;
    }

    //region InteractorConsumer Delegate
    @Override
    @SuppressWarnings("unchecked")
    public void accept(Object data) {
        if (data != null) {
            if (data instanceof Error) {
                if (mCallback != null) {
                    //report error
                    mCallback.onError((Error) data);
                }
            } else if (data instanceof List) {
                if (mCallback != null) {
                    //load result
                    mCallback.onResult((List<NewsEntity>) data);
                }
            }
        }
    }
    //endregion
}

