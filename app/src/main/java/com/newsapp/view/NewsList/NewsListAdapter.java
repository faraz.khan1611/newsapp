package com.newsapp.view.NewsList;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.newsapp.ModelLayer.Entity.MediaEntity;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import com.newsapp.ModelLayer.Entity.NewsEntity;
import news.agoda.com.sample.R;

class NewsListAdapter extends ArrayAdapter {
    private static class ViewHolder {
        TextView newsTitle;
        DraweeView imageView;
    }

    @SuppressWarnings("unchecked")
    NewsListAdapter(Context context, @SuppressWarnings("SameParameterValue") int resource, List<NewsEntity> objects) {
        super(context, resource, objects);
    }

    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        NewsEntity newsEntity = (NewsEntity) getItem(position);
        List<MediaEntity> mediaEntityList = Objects.requireNonNull(newsEntity).getMediaEntity();
        String thumbnailURL = "";
        if(!mediaEntityList.isEmpty())
        {
            MediaEntity mediaEntity = mediaEntityList.get(0);
            thumbnailURL = mediaEntity.getUrl();
        }

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_news, parent, false);
            viewHolder.newsTitle = (TextView) convertView.findViewById(R.id.news_title);
            viewHolder.imageView = (DraweeView) convertView.findViewById(R.id.news_item_image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.newsTitle.setText(newsEntity.getTitle());
        DraweeController draweeController = Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequest.fromUri
                (Uri.parse(thumbnailURL))).setOldController(viewHolder.imageView.getController()).build();
        viewHolder.imageView.setController(draweeController);
        return convertView;
    }
}
