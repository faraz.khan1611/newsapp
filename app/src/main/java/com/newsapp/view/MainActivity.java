package com.newsapp.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.newsapp.Dependencies.DependencyRegistry;
import com.newsapp.view.NewsList.NewsListFragment;
import com.newsapp.view.NewsList.NewsListPresenter;
import news.agoda.com.sample.R;
import com.newsapp.Router.Router;

public class MainActivity extends Activity implements NewsListFragment.OnNewsItemSelectedListener {

    private NewsListPresenter mPresenter;
    private Router mRouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_list_fragment);
        DependencyRegistry.getInstance().inject(this);
    }

    //region Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region Initialize
    public void configurePresenter(NewsListPresenter presenter, Router router) {
        this.mPresenter = presenter;
        this.mRouter = router;
        loadInitialView();
    }

    private void loadInitialView() {
        if (findViewById(R.id.fragment_container) != null) {
            NewsListFragment newsFragment = new NewsListFragment();
            newsFragment.setArguments(getIntent().getExtras());
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, newsFragment, NewsListFragment.TAG)
                    .commit();
            newsFragment.setPresenter(mPresenter);
        } else {
            NewsListFragment newsFragment = (NewsListFragment) getFragmentManager().findFragmentById(R.id.news_list_fragment);
            if (newsFragment != null) {
                newsFragment.setPresenter(mPresenter);
            }
        }

    }
    //endregion

    //region Fragment callback
    @Override
    public void onNewsItemClick(String title, String storyURL, String summary, String imageURL) {
        mRouter.handleNewsListItemClick(getFragmentManager(), title, storyURL, summary, imageURL);

    }
    //endregion

}
