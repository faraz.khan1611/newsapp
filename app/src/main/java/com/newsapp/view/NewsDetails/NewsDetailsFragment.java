package com.newsapp.view.NewsDetails;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.newsapp.Helper.Constants;

import com.newsapp.Dependencies.DependencyRegistry;
import news.agoda.com.sample.R;
import com.newsapp.Router.Router;

public class NewsDetailsFragment extends Fragment {
    public static final String TAG = NewsDetailsFragment.class.getName();
    private String mStoryURL = "";
    private Router mRouter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DependencyRegistry.getInstance().inject(this);
        return inflater.inflate(R.layout.news_detail_fragment, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            // Set article based on argument passed in
            updateNewsDetailView(args);
        }
    }

    public void configureRouter(Router router) {
        this.mRouter = router;
    }

    public void updateNewsDetailView(Bundle extras) {
        String title = Constants.EMPTY_TITLE;
        String summary = Constants.EMPTY_SUMMARY;
        String imageURL = "";
        if (extras != null) {
            mStoryURL = getStringFromExtra(extras, Constants.STORY_URL);
            title = getStringFromExtra(extras, Constants.TITLE);
            summary = getStringFromExtra(extras, Constants.SUMMARY);
            imageURL = getStringFromExtra(extras, Constants.IMAGE_URL);
        }

        TextView titleView = (TextView) getActivity().findViewById(R.id.title);
        DraweeView imageView = (DraweeView) getActivity().findViewById(R.id.news_image);
        TextView summaryView = (TextView) getActivity().findViewById(R.id.summary_content);
        Button fullStoryButton = (Button) getActivity().findViewById(R.id.full_story_link);
        fullStoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFullStoryClicked();
            }
        });
        if(!mStoryURL.isEmpty())
        {
            fullStoryButton.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
        }
        titleView.setText(title);
        summaryView.setText(summary);

        DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setImageRequest(ImageRequest.fromUri(Uri.parse(imageURL)))
                .setOldController(imageView.getController()).build();
        imageView.setController(draweeController);
    }

    private String getStringFromExtra(Bundle extras, String key) {
        String value = "";
        if (extras.containsKey(key)) {
            value = extras.getString(key);
        }
        return value;
    }

    @SuppressWarnings("unused")
    private void onFullStoryClicked() {
        boolean isSuccess = this.mRouter.onFullStoryClicked(getActivity(), mStoryURL);
    }
}
