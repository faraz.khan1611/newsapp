package com.newsapp.Helper;

public class Constants {
    public final static String NEWS_URL = "https://api.myjson.com/bins/nl6jh";

    //region detail bundle key
    public final static String STORY_URL = "STORY_URL";
    public final static String TITLE = "TITLE";
    public final static String SUMMARY = "SUMMARY";
    public final static String IMAGE_URL = "IMAGE_URL";
    //endregion

    public final static String EMPTY_TITLE = "Title not present";
    public final static String EMPTY_SUMMARY = "Summary not present";

    //region JSON Keys
    public final static String JSON_TITLE = "title";
    public final static String JSON_ABSTRACT = "abstract";
    public final static String JSON_ARTICLE_URL = "url";
    public final static String JSON_BYLINE = "byline";
    public final static String JSON_PUBLISHED_DATE = "published_date";
    public final static String JSON_MULTIMEDIA = "multimedia";

    public final static String JSON_MEDIA_URL = "url";
    public final static String JSON_FORMAT = "format";
    public final static String JSON_HEIGHT = "height";
    public final static String JSON_WIDTH = "width";
    public final static String JSON_TYPE = "type";
    public final static String JSON_SUBTYPE = "subtype";
    public final static String JSON_CAPTION = "caption";
    public final static String JSON_COPYRIGHT = "copyright";

    public final static String JSON_RESULTS = "results";
    //endregion


}
