package com.newsapp.Helper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtils {

    public static String getStringFromJSON(@Nullable JSONObject json, @NonNull String key) {
        String value = null;
        if (hasKey(json, key)) {
            try {
                value = json.getString(key);
                if (value == null || value.equals("null")) {
                    return null;
                }
            } catch (JSONException ignore) {
            }
        }
        return value;
    }

    public static int getIntFromJSON(@Nullable JSONObject json, @NonNull String key, int defaultValue) {
        int value = defaultValue;
        if (hasKey(json, key)) {
            try {
                value = json.getInt(key);
            } catch (JSONException ignore) {
                //getInt will try coerce the value to an int, if it fails there's nothing we can do except return default
                //This coercing needs to be tested with JSONObject .. instrumented test
            }
        }
        return value;
    }

    private static boolean hasKey(@Nullable JSONObject json, @NonNull String key) {
        return json != null && json.has(key);
    }
}
