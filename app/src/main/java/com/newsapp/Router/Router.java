package com.newsapp.Router;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.newsapp.Helper.Constants;
import com.newsapp.view.NewsDetails.NewsDetailsFragment;

import org.jetbrains.annotations.NotNull;

import news.agoda.com.sample.R;

public class Router {

    public void handleNewsListItemClick(@NotNull FragmentManager manager, String title, String storyURL, String summary, String imageURL) {
        NewsDetailsFragment detailsFrag = (NewsDetailsFragment)
                manager.findFragmentById(R.id.news_details_fragment);
        Bundle args = new Bundle();
        args.putString(Constants.TITLE, title);
        args.putString(Constants.STORY_URL, storyURL);
        args.putString(Constants.SUMMARY, summary);
        args.putString(Constants.IMAGE_URL, imageURL);
        if (detailsFrag != null) {
            // we're in two-pane layout...
            detailsFrag.updateNewsDetailView(args);
        } else {
            // we're in the one-pane layout
            NewsDetailsFragment newFragment = new NewsDetailsFragment();
            newFragment.setArguments(args);
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.fragment_container, newFragment, NewsDetailsFragment.TAG);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public boolean onFullStoryClicked(@NotNull Context context, String storyURL) {
        if (storyURL.isEmpty()) {
            return false;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(storyURL));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
            return true;
        }
        return false;
    }
}
