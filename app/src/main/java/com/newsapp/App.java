package com.newsapp;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

@SuppressWarnings("WeakerAccess")
public class App extends Application {
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
