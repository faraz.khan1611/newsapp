package com.newsapp.ModelLayer.Entity;

import android.util.Log;

import com.newsapp.Helper.Constants;
import com.newsapp.Helper.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This represents a news item
 */
public class NewsEntity {
    private static final String TAG = NewsEntity.class.getSimpleName();
    private String title;
    private String summary;
    private String articleUrl;
    private String byline;
    private String publishedDate;
    private List<MediaEntity> mediaEntityList;

    public NewsEntity(JSONObject jsonObject) {
        try {
            mediaEntityList = new ArrayList<>();
            title = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_TITLE);
            summary = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_ABSTRACT);
            articleUrl = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_ARTICLE_URL);
            byline = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_BYLINE);
            publishedDate = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_PUBLISHED_DATE);
            if (jsonObject.has(Constants.JSON_MULTIMEDIA)) {
                if (jsonObject.get(Constants.JSON_MULTIMEDIA) instanceof JSONArray) {
                    JSONArray mediaArray = jsonObject.getJSONArray(Constants.JSON_MULTIMEDIA);
                    for (int i = 0; i < mediaArray.length(); i++) {
                        if (mediaArray.get(i) instanceof JSONObject) {
                            JSONObject mediaObject = mediaArray.getJSONObject(i);
                            MediaEntity mediaEntity = new MediaEntity(mediaObject);
                            mediaEntityList.add(mediaEntity);
                        }
                    }
                }
            }

        } catch (JSONException exception) {
            Log.e(TAG, exception.getMessage());
        }
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    @SuppressWarnings("unused")
    String getByline() {
        return byline;
    }

    @SuppressWarnings("unused")
    String getPublishedDate() {
        return publishedDate;
    }

    public List<MediaEntity> getMediaEntity() {
        return mediaEntityList;
    }
}
