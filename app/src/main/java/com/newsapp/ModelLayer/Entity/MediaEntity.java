package com.newsapp.ModelLayer.Entity;

import android.support.annotation.NonNull;

import com.newsapp.Helper.Constants;

import org.json.JSONObject;

import com.newsapp.Helper.JSONUtils;

/**
 * This class represents a media item
 */
public class MediaEntity {
    private final String url;
    private final String format;
    private final int height;
    private final int width;
    private final String type;
    private final String subType;
    private final String caption;
    private final String copyright;

    MediaEntity(@NonNull JSONObject jsonObject) {
        url = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_MEDIA_URL);
        format = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_FORMAT);
        height = JSONUtils.getIntFromJSON(jsonObject, Constants.JSON_HEIGHT, 0);
        width = JSONUtils.getIntFromJSON(jsonObject, Constants.JSON_WIDTH, 0);
        type = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_TYPE);
        subType = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_SUBTYPE);
        caption = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_CAPTION);
        copyright = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_COPYRIGHT);
    }

    public String getUrl() {
        return url;
    }

    @SuppressWarnings("unused")
    String getFormat() {
        return format;
    }

    @SuppressWarnings("unused")
    int getHeight() {
        return height;
    }

    @SuppressWarnings("unused")
    int getWidth() {
        return width;
    }

    @SuppressWarnings("unused")
    String getType() {
        return type;
    }

    @SuppressWarnings("unused")
    String getSubType() {
        return subType;
    }

    @SuppressWarnings("unused")
    String getCaption() {
        return caption;
    }

    @SuppressWarnings("unused")
    public String getCopyright() {
        return copyright;
    }

}
