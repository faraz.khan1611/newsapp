package com.newsapp.ModelLayer;

@SuppressWarnings("SpellCheckingInspection")
public interface InteractorLayer {
    void loadNewsResource(InteractorConsumer consumer);
}
