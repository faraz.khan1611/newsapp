package com.newsapp.ModelLayer;

public interface InteractorConsumer<T> {
    void accept(Object data);
}
