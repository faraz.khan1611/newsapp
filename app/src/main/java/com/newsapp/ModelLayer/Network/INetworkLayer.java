package com.newsapp.ModelLayer.Network;

import org.json.JSONObject;

public interface INetworkLayer {
    void fetchDataWithURL(final String URL, final INetworkCallbackInterface<JSONObject> networkCallback);
}