package com.newsapp.ModelLayer.Network;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkLayerImpl implements INetworkLayer {

    @Override
    public void fetchDataWithURL(final String URL, final INetworkCallbackInterface<JSONObject> networkCallback) {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    String readStream = readStream(con.getInputStream());
                    JSONObject json;
                    json = new JSONObject(readStream);
                    if (networkCallback != null) {
                        networkCallback.onSuccess(json);
                    }
                } catch (Exception e) {
                    if (networkCallback != null) {
                        networkCallback.onError(new Error(e.getMessage()));
                    }
                }
            }
        };
        AsyncOperation.postAsync(task);
    }

    private String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {

            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
