package com.newsapp.ModelLayer.Network;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class AsyncOperation {

    private final static ExecutorService mExecutorService = Executors.newCachedThreadPool();

    static void postAsync(final Runnable runnable) {
        mExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                runnable.run();
            }
        });
    }
}
