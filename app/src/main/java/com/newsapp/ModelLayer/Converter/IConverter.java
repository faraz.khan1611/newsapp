package com.newsapp.ModelLayer.Converter;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.List;

import com.newsapp.ModelLayer.Entity.NewsEntity;

public interface IConverter {
    List<NewsEntity> convertToNewsEntityList(@NotNull JSONObject result);
}
