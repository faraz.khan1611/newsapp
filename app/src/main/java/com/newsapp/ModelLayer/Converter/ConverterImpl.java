package com.newsapp.ModelLayer.Converter;


import com.newsapp.Helper.Constants;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.newsapp.ModelLayer.Entity.NewsEntity;

public class ConverterImpl implements IConverter {

    @Override
    public List<NewsEntity> convertToNewsEntityList(@NotNull JSONObject result) {
        List<NewsEntity> list = new ArrayList<>();
        JSONArray resultArray;
        try {
            if (result.has(Constants.JSON_RESULTS)) {
                resultArray = result.getJSONArray(Constants.JSON_RESULTS);
                for (int i = 0; i < resultArray.length(); i++) {
                    if (resultArray.get(i) instanceof JSONObject) {
                        JSONObject newsObject = resultArray.getJSONObject(i);
                        NewsEntity newsEntity = new NewsEntity(newsObject);
                        list.add(newsEntity);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
}
