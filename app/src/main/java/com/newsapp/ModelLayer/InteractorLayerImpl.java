package com.newsapp.ModelLayer;

import com.newsapp.Helper.Constants;
import com.newsapp.ModelLayer.Network.INetworkCallbackInterface;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.List;

import com.newsapp.ModelLayer.Converter.IConverter;
import com.newsapp.ModelLayer.Entity.NewsEntity;
import com.newsapp.ModelLayer.Network.Error;
import com.newsapp.ModelLayer.Network.INetworkLayer;

public class InteractorLayerImpl implements InteractorLayer {

    private final INetworkLayer mNetworkLayer;
    private final IConverter mTranslationLayer;

    public InteractorLayerImpl(INetworkLayer networkLayer, IConverter translationLayer) {
        this.mNetworkLayer = networkLayer;
        this.mTranslationLayer = translationLayer;
    }

    @Override
    public void loadNewsResource(InteractorConsumer callback) {
        final WeakReference<IConverter> converter = new WeakReference<>(mTranslationLayer);
        final WeakReference<InteractorConsumer> listener = new WeakReference<>(callback);
        this.mNetworkLayer.fetchDataWithURL(Constants.NEWS_URL,new INetworkCallbackInterface<JSONObject>() {

            @Override
            public void onSuccess(JSONObject result) {
                IConverter convert = converter.get();
                if (convert != null) {
                    List<NewsEntity> entity = convert.convertToNewsEntityList(result);
                    InteractorConsumer cb = listener.get();
                    if (cb != null) {
                        cb.accept(entity);
                    }
                }
            }

            @Override
            public void onError(Error error) {
                InteractorConsumer cb = listener.get();
                if (cb != null) {
                    cb.accept(error);
                }
            }
        });
    }
}
