package com.newsapp.Dependencies;

import com.newsapp.ModelLayer.Converter.ConverterImpl;
import com.newsapp.ModelLayer.InteractorLayer;
import com.newsapp.ModelLayer.InteractorLayerImpl;
import com.newsapp.ModelLayer.Network.INetworkLayer;
import com.newsapp.ModelLayer.Network.NetworkLayerImpl;
import com.newsapp.Router.Router;
import com.newsapp.view.MainActivity;
import com.newsapp.view.NewsDetails.NewsDetailsFragment;

import org.jetbrains.annotations.NotNull;

import com.newsapp.view.NewsList.NewsListPresenter;
import com.newsapp.view.NewsList.NewsListPresenterImpl;
import com.newsapp.ModelLayer.Converter.IConverter;

public class DependencyRegistry {
    private static class Singleton {
        private static final DependencyRegistry INSTANCE = new DependencyRegistry();
    }

    public static DependencyRegistry getInstance() {
        return DependencyRegistry.Singleton.INSTANCE;
    }

    //region Singletons

    private final IConverter mTranslationLayer = createConverter();

    private IConverter createConverter() {
        return new ConverterImpl();
    }

    private final INetworkLayer mNetworkLayer = new NetworkLayerImpl();

    private final InteractorLayer mInteractorLayer = createInteractorLayer();

    private InteractorLayer createInteractorLayer() {
        return new InteractorLayerImpl(mNetworkLayer, mTranslationLayer);
    }

    //endregion

    //region Coordinators

    private final Router mRouter = new Router();

    //endregion

    //region Injection Methods
    public void inject(@NotNull MainActivity activity) {
        NewsListPresenter presenter = new NewsListPresenterImpl(mInteractorLayer);
        activity.configurePresenter(presenter, mRouter);
    }

    public void inject(@NotNull NewsDetailsFragment detailFragment) {
        detailFragment.configureRouter(mRouter);
    }
    //endregion


}
