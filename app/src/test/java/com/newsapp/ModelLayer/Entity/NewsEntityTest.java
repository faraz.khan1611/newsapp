package com.newsapp.ModelLayer.Entity;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.newsapp.Helper.Constants;
import com.newsapp.ModelLayer.Entity.NewsEntity;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NewsEntityTest {

    private final String mockTitle = "Work Policies May Be Kinder, but Brutal Competition Isn’t";
    private final String mockSummary = "Top-tier employers may be changing their official policies in a nod to work-life balance";
    private final String mockArticleUrl = "http://www.nytimes.com/2015/08/18/business/work";
    private final String mockByline = "by dev";
    private final String mockPublishedDate = "2015-08-17T22:10:02-5:00";


    private final String mockUrl = "http://static01.nyt.com/images/2015/08/18/business/18EMPLOY/18EMPLOY-thumbStandard.jpg";
    private final int mockHeight = 75;
    private final String mockType = "image";
    private final String mockSubType = "photo";
    private final String mockCaption = "People eating at the Brave Horse Tavern on the Amazon campus in Seattle in June.";
    private final String mockCopyright = "Matthew Ryan Williams for The New York Times";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void testNewsEntity_AllKeyAndValue() throws Exception {

        JSONObject jsonObjectMock =  mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_ABSTRACT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ABSTRACT)).thenReturn(mockSummary);

        when(jsonObjectMock.has(Constants.JSON_ARTICLE_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ARTICLE_URL)).thenReturn(mockArticleUrl);

        when(jsonObjectMock.has(Constants.JSON_BYLINE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_BYLINE)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_PUBLISHED_DATE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_PUBLISHED_DATE)).thenReturn(mockPublishedDate);

        //region multimedia array
        JSONArray mockMultimediaJSONArray = mock(JSONArray.class);
        JSONObject mediaJSON =  mock(JSONObject.class);

        when(mediaJSON.has(Constants.JSON_MEDIA_URL)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_MEDIA_URL)).thenReturn(mockUrl);

        when(mediaJSON.has(Constants.JSON_FORMAT)).thenReturn(true);
        String formatJSON = "Standard Thumbnail";
        when(mediaJSON.getString(Constants.JSON_FORMAT)).thenReturn(formatJSON);

        when(mediaJSON.has(Constants.JSON_HEIGHT)).thenReturn(true);
        when(mediaJSON.getInt(Constants.JSON_HEIGHT)).thenReturn(mockHeight);

        when(mediaJSON.has(Constants.JSON_WIDTH)).thenReturn(true);
        int mockWidth = 75;
        when(mediaJSON.getInt(Constants.JSON_WIDTH)).thenReturn(mockWidth);

        when(mediaJSON.has(Constants.JSON_TYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_TYPE)).thenReturn(mockType);

        when(mediaJSON.has(Constants.JSON_SUBTYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_SUBTYPE)).thenReturn(mockSubType);

        when(mediaJSON.has(Constants.JSON_CAPTION)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_CAPTION)).thenReturn(mockCaption);

        when(mediaJSON.has(Constants.JSON_COPYRIGHT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_COPYRIGHT)).thenReturn(mockCopyright);

        doReturn(1)
                .when(mockMultimediaJSONArray).length();

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).get(0);

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).getJSONObject(0);

        when(jsonObjectMock.has(Constants.JSON_MULTIMEDIA)).thenReturn(true);
        when(jsonObjectMock.get(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        when(jsonObjectMock.getJSONArray(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getSummary(),is(equalTo(mockSummary)));
        assertThat(newsEntity.getArticleUrl(),is(equalTo(mockArticleUrl)));
        assertThat(newsEntity.getByline(),is(equalTo(mockByline)));
        assertThat(newsEntity.getPublishedDate(),is(equalTo(mockPublishedDate)));
        assertThat(newsEntity.getMediaEntity().size(),is(1));

        assertThat(newsEntity.getMediaEntity().get(0).getUrl(),is(equalTo(mockUrl)));
        assertThat(newsEntity.getMediaEntity().get(0).getFormat(),is(equalTo(formatJSON)));
        assertThat(newsEntity.getMediaEntity().get(0).getHeight(),is(mockHeight));
        assertThat(newsEntity.getMediaEntity().get(0).getWidth(),is(mockWidth));
        assertThat(newsEntity.getMediaEntity().get(0).getType(),is(equalTo(mockType)));
        assertThat(newsEntity.getMediaEntity().get(0).getSubType(),is(equalTo(mockSubType)));
        assertThat(newsEntity.getMediaEntity().get(0).getCaption(),is(equalTo(mockCaption)));
        assertThat(newsEntity.getMediaEntity().get(0).getCopyright(),is(equalTo(mockCopyright)));
    }

    @Test
    public void testNewsEntity_MissingKeyAndValue() throws Exception {

        JSONObject jsonObjectMock =  mock(JSONObject.class);

        //Missing title
        when(jsonObjectMock.has(Constants.JSON_TITLE)).thenReturn(false);

        when(jsonObjectMock.has(Constants.JSON_ABSTRACT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ABSTRACT)).thenReturn(mockSummary);

        when(jsonObjectMock.has(Constants.JSON_ARTICLE_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ARTICLE_URL)).thenReturn(mockArticleUrl);

        //Missing TITLE
        when(jsonObjectMock.has(Constants.JSON_BYLINE)).thenReturn(false);

        when(jsonObjectMock.has(Constants.JSON_PUBLISHED_DATE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_PUBLISHED_DATE)).thenReturn(mockPublishedDate);

        //region multimedia array
        JSONArray mockMultimediaJSONArray = mock(JSONArray.class);
        JSONObject mediaJSON =  mock(JSONObject.class);

        when(mediaJSON.has(Constants.JSON_MEDIA_URL)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_MEDIA_URL)).thenReturn(mockUrl);

        //Missing FORMAT
        when(mediaJSON.has(Constants.JSON_FORMAT)).thenReturn(false);

        when(mediaJSON.has(Constants.JSON_HEIGHT)).thenReturn(true);
        when(mediaJSON.getInt(Constants.JSON_HEIGHT)).thenReturn(mockHeight);

        //Missing WIDTH
        when(mediaJSON.has(Constants.JSON_WIDTH)).thenReturn(false);

        when(mediaJSON.has(Constants.JSON_TYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_TYPE)).thenReturn(mockType);

        when(mediaJSON.has(Constants.JSON_SUBTYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_SUBTYPE)).thenReturn(mockSubType);

        when(mediaJSON.has(Constants.JSON_CAPTION)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_CAPTION)).thenReturn(mockCaption);

        when(mediaJSON.has(Constants.JSON_COPYRIGHT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_COPYRIGHT)).thenReturn(mockCopyright);

        doReturn(1)
                .when(mockMultimediaJSONArray).length();

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).get(0);

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).getJSONObject(0);

        when(jsonObjectMock.has(Constants.JSON_MULTIMEDIA)).thenReturn(true);
        when(jsonObjectMock.get(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        when(jsonObjectMock.getJSONArray(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(null)));
        assertThat(newsEntity.getSummary(),is(equalTo(mockSummary)));
        assertThat(newsEntity.getArticleUrl(),is(equalTo(mockArticleUrl)));
        assertThat(newsEntity.getByline(),is(equalTo(null)));
        assertThat(newsEntity.getPublishedDate(),is(equalTo(mockPublishedDate)));
        assertThat(newsEntity.getMediaEntity().size(),is(1));

        assertThat(newsEntity.getMediaEntity().get(0).getUrl(),is(equalTo(mockUrl)));
        assertThat(newsEntity.getMediaEntity().get(0).getFormat(),is(equalTo(null)));
        assertThat(newsEntity.getMediaEntity().get(0).getHeight(),is(mockHeight));
        assertThat(newsEntity.getMediaEntity().get(0).getWidth(),is(0));
        assertThat(newsEntity.getMediaEntity().get(0).getType(),is(equalTo(mockType)));
        assertThat(newsEntity.getMediaEntity().get(0).getSubType(),is(equalTo(mockSubType)));
        assertThat(newsEntity.getMediaEntity().get(0).getCaption(),is(equalTo(mockCaption)));
        assertThat(newsEntity.getMediaEntity().get(0).getCopyright(),is(equalTo(mockCopyright)));
    }

    @Test
    public void testNewsEntity_TestCrash_MultimediaArrayMissing() throws Exception {

        JSONObject jsonObjectMock =  mock(JSONObject.class);

        when(jsonObjectMock.has(Constants.JSON_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_ABSTRACT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ABSTRACT)).thenReturn(mockSummary);

        when(jsonObjectMock.has(Constants.JSON_ARTICLE_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ARTICLE_URL)).thenReturn(mockArticleUrl);

        when(jsonObjectMock.has(Constants.JSON_BYLINE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_BYLINE)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_PUBLISHED_DATE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_PUBLISHED_DATE)).thenReturn(mockPublishedDate);

        //Here we check if key is not present in json, this crash was fix in the initial codebase,
        //Added test to make sure not repeated in future
        when(jsonObjectMock.has(Constants.JSON_MULTIMEDIA)).thenReturn(false);

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getSummary(),is(equalTo(mockSummary)));
        assertThat(newsEntity.getArticleUrl(),is(equalTo(mockArticleUrl)));
        assertThat(newsEntity.getByline(),is(equalTo(mockByline)));
        assertThat(newsEntity.getPublishedDate(),is(equalTo(mockPublishedDate)));
        assertThat(newsEntity.getMediaEntity().size(),is(0));
    }

    @Test
    public void testNewsEntity_TestCrash_MultimediaArrayNotTypeOfJSONArray() throws Exception {

        JSONObject jsonObjectMock =  mock(JSONObject.class);

        when(jsonObjectMock.has(Constants.JSON_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_ABSTRACT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ABSTRACT)).thenReturn(mockSummary);

        when(jsonObjectMock.has(Constants.JSON_ARTICLE_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_ARTICLE_URL)).thenReturn(mockArticleUrl);

        when(jsonObjectMock.has(Constants.JSON_BYLINE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_BYLINE)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_PUBLISHED_DATE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_PUBLISHED_DATE)).thenReturn(mockPublishedDate);

        //Here we returned json object instead of json array for key multimedia
        when(jsonObjectMock.has(Constants.JSON_MULTIMEDIA)).thenReturn(true);
        when(jsonObjectMock.get(Constants.JSON_MULTIMEDIA)).thenReturn(any(JSONObject.class));

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getSummary(),is(equalTo(mockSummary)));
        assertThat(newsEntity.getArticleUrl(),is(equalTo(mockArticleUrl)));
        assertThat(newsEntity.getByline(),is(equalTo(mockByline)));
        assertThat(newsEntity.getPublishedDate(),is(equalTo(mockPublishedDate)));
        assertThat(newsEntity.getMediaEntity().size(),is(0));
    }
}
