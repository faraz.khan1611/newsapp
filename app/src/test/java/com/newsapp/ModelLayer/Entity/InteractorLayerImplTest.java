package com.newsapp.ModelLayer.Entity;

import org.hamcrest.CoreMatchers;
import org.hamcrest.core.Is;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import com.newsapp.Helper.Constants;
import com.newsapp.ModelLayer.Converter.ConverterImpl;
import com.newsapp.ModelLayer.Converter.IConverter;
import com.newsapp.ModelLayer.Entity.NewsEntity;
import com.newsapp.ModelLayer.InteractorConsumer;
import com.newsapp.ModelLayer.InteractorLayer;
import com.newsapp.ModelLayer.InteractorLayerImpl;
import com.newsapp.ModelLayer.Network.Error;
import com.newsapp.ModelLayer.Network.INetworkCallbackInterface;
import com.newsapp.ModelLayer.Network.INetworkLayer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InteractorLayerImplTest {

    private final String mockTitle = "Work Policies May Be Kinder, but Brutal Competition Isn’t";
    private final String mockSummary = "Top-tier employers may be changing their official policies in a nod to work-life balance";
    private final String mockArticleUrl = "http://www.nytimes.com/2015/08/18/business/work";


    private final String mockUrl = "http://static01.nyt.com/images/2015/08/18/business/18EMPLOY/18EMPLOY-thumbStandard.jpg";
    private final int mockHeight = 75;
    private final int mockWidth = 75;
    private final String mockType = "image";
    private final String mockSubType = "photo";
    private final String mockCaption = "People eating at the Brave Horse Tavern on the Amazon campus in Seattle in June.";
    private final String mockCopyright = "Matthew Ryan Williams for The New York Times";
    private final String formatJSON = "Standard Thumbnail";

    @Captor
    private ArgumentCaptor<INetworkCallbackInterface<JSONObject>> callbackCaptor;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void loadNewsResource_Failure() {
        INetworkLayer networkLayer = mock(INetworkLayer.class);
        IConverter translationLayer = mock(ConverterImpl.class);
        InteractorLayer interactorLayer = new InteractorLayerImpl(networkLayer,translationLayer);
        doNothing().when(networkLayer).fetchDataWithURL(anyString(),callbackCaptor.capture());
        interactorLayer.loadNewsResource(new InteractorConsumer() {
            @Override
            public void accept(Object data) {
                assertThat(data,instanceOf(Error.class));
                Error error = (Error) data;
                assertThat(error.getMessage(),equalTo("Test Error"));
            }
        });

        callbackCaptor.getValue().onError(new Error("Test Error"));

        verify(translationLayer,times(0)).convertToNewsEntityList(any(JSONObject.class));
    }


    @Test
    public void loadNewsResource_Success() throws JSONException {
        INetworkLayer networkLayer = mock(INetworkLayer.class);
        IConverter translationLayer = mock(ConverterImpl.class);
        InteractorLayer interactorLayer = new InteractorLayerImpl(networkLayer,translationLayer);
        doNothing().when(networkLayer).fetchDataWithURL(anyString(),callbackCaptor.capture());
        interactorLayer.loadNewsResource(new InteractorConsumer() {
            @SuppressWarnings("unchecked")
            @Override
            public void accept(Object data) {
                assertThat(data,instanceOf(List.class));

                List<NewsEntity> list = (List<NewsEntity>)data;
                assertThat(list.size(), Is.is(1));

                //region verify newsEntity from the list
                NewsEntity newsEntity = list.get(0);

                assertThat(newsEntity.getTitle(), CoreMatchers.is(equalTo(mockTitle)));
                assertThat(newsEntity.getSummary(), CoreMatchers.is(equalTo(mockSummary)));
                assertThat(newsEntity.getArticleUrl(), CoreMatchers.is(equalTo(mockArticleUrl)));
                assertThat(newsEntity.getMediaEntity().size(), CoreMatchers.is(1));

                assertThat(newsEntity.getMediaEntity().get(0).getUrl(), CoreMatchers.is(equalTo(mockUrl)));
                assertThat(newsEntity.getMediaEntity().get(0).getFormat(), CoreMatchers.is(equalTo(formatJSON)));
                assertThat(newsEntity.getMediaEntity().get(0).getHeight(), CoreMatchers.is(mockHeight));
                assertThat(newsEntity.getMediaEntity().get(0).getWidth(), CoreMatchers.is(mockWidth));
                assertThat(newsEntity.getMediaEntity().get(0).getType(), CoreMatchers.is(equalTo(mockType)));
                assertThat(newsEntity.getMediaEntity().get(0).getSubType(), CoreMatchers.is(equalTo(mockSubType)));
                assertThat(newsEntity.getMediaEntity().get(0).getCaption(), CoreMatchers.is(equalTo(mockCaption)));
                assertThat(newsEntity.getMediaEntity().get(0).getCopyright(), CoreMatchers.is(equalTo(mockCopyright)));
                //endregion
            }
        });
        doCallRealMethod().when(translationLayer).convertToNewsEntityList(any(JSONObject.class));
        JSONObject mockObject = prepareMockResultJSONArrayToConvert();
        callbackCaptor.getValue().onSuccess(mockObject);
        verify(translationLayer,atLeastOnce()).convertToNewsEntityList(mockObject);
    }

    private JSONObject prepareMockResultJSONArrayToConvert() throws JSONException {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        //region result array
        JSONArray resultJSONArrayMock = mock(JSONArray.class);
        JSONObject newsEntityJson = mock(JSONObject.class);

        //region newsEntityJSON
        when(newsEntityJson.has(Constants.JSON_TITLE)).thenReturn(true);
        when(newsEntityJson.getString(Constants.JSON_TITLE)).thenReturn(mockTitle);

        when(newsEntityJson.has(Constants.JSON_ABSTRACT)).thenReturn(true);
        when(newsEntityJson.getString(Constants.JSON_ABSTRACT)).thenReturn(mockSummary);

        when(newsEntityJson.has(Constants.JSON_ARTICLE_URL)).thenReturn(true);
        when(newsEntityJson.getString(Constants.JSON_ARTICLE_URL)).thenReturn(mockArticleUrl);

        when(newsEntityJson.has(Constants.JSON_BYLINE)).thenReturn(true);
        String mockByline = "by dev";
        when(newsEntityJson.getString(Constants.JSON_BYLINE)).thenReturn(mockByline);

        when(newsEntityJson.has(Constants.JSON_PUBLISHED_DATE)).thenReturn(true);
        String mockPublishedDate = "2015-08-17T22:10:02-5:00";
        when(newsEntityJson.getString(Constants.JSON_PUBLISHED_DATE)).thenReturn(mockPublishedDate);

        //region multimedia array
        JSONArray mockMultimediaJSONArray = mock(JSONArray.class);
        JSONObject mediaJSON = mock(JSONObject.class);

        when(mediaJSON.has(Constants.JSON_MEDIA_URL)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_MEDIA_URL)).thenReturn(mockUrl);

        when(mediaJSON.has(Constants.JSON_FORMAT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_FORMAT)).thenReturn(formatJSON);

        when(mediaJSON.has(Constants.JSON_HEIGHT)).thenReturn(true);
        when(mediaJSON.getInt(Constants.JSON_HEIGHT)).thenReturn(mockHeight);

        when(mediaJSON.has(Constants.JSON_WIDTH)).thenReturn(true);
        when(mediaJSON.getInt(Constants.JSON_WIDTH)).thenReturn(mockWidth);

        when(mediaJSON.has(Constants.JSON_TYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_TYPE)).thenReturn(mockType);

        when(mediaJSON.has(Constants.JSON_SUBTYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_SUBTYPE)).thenReturn(mockSubType);

        when(mediaJSON.has(Constants.JSON_CAPTION)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_CAPTION)).thenReturn(mockCaption);

        when(mediaJSON.has(Constants.JSON_COPYRIGHT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_COPYRIGHT)).thenReturn(mockCopyright);

        doReturn(1)
                .when(mockMultimediaJSONArray).length();

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).get(0);

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).getJSONObject(0);

        when(newsEntityJson.has(Constants.JSON_MULTIMEDIA)).thenReturn(true);
        when(newsEntityJson.get(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        when(newsEntityJson.getJSONArray(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        //endregion


        doReturn(1)
                .when(resultJSONArrayMock).length();

        doReturn(newsEntityJson)
                .when(resultJSONArrayMock).get(0);

        doReturn(newsEntityJson)
                .when(resultJSONArrayMock).getJSONObject(0);
        //endregion

        when(jsonObjectMock.has(Constants.JSON_RESULTS)).thenReturn(true);
        when(jsonObjectMock.getJSONArray(Constants.JSON_RESULTS)).thenReturn(resultJSONArrayMock);
        //endregion

        return jsonObjectMock;

    }


}
