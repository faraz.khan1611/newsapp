package com.newsapp.ModelLayer.Entity;

import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.newsapp.Helper.Constants;
import com.newsapp.ModelLayer.Entity.MediaEntity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MediaEntityTest {

    private final int mockWidth = 75;
    private final String mockType = "image";
    private final String mockCaption = "People eating at the Brave Horse Tavern on the Amazon campus in Seattle in June.";
    private final String mockCopyright = "Matthew Ryan Williams for The New York Times";
    private final String mockFormatJSON = "Standard Thumbnail";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void testMediaEntity_AllKeyAndValue() throws Exception {
        JSONObject mediaJSON =  mock(JSONObject.class);

        when(mediaJSON.has(Constants.JSON_MEDIA_URL)).thenReturn(true);
        String mockUrl = "http://static01.nyt.com/images/2015/08/18/business/18EMPLOY/18EMPLOY-thumbStandard.jpg";
        when(mediaJSON.getString(Constants.JSON_MEDIA_URL)).thenReturn(mockUrl);

        when(mediaJSON.has(Constants.JSON_FORMAT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_FORMAT)).thenReturn(mockFormatJSON);

        when(mediaJSON.has(Constants.JSON_HEIGHT)).thenReturn(true);
        int mockHeight = 75;
        when(mediaJSON.getInt(Constants.JSON_HEIGHT)).thenReturn(mockHeight);

        when(mediaJSON.has(Constants.JSON_WIDTH)).thenReturn(true);
        when(mediaJSON.getInt(Constants.JSON_WIDTH)).thenReturn(mockWidth);

        when(mediaJSON.has(Constants.JSON_TYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_TYPE)).thenReturn(mockType);

        when(mediaJSON.has(Constants.JSON_SUBTYPE)).thenReturn(true);
        String mockSubType = "photo";
        when(mediaJSON.getString(Constants.JSON_SUBTYPE)).thenReturn(mockSubType);

        when(mediaJSON.has(Constants.JSON_CAPTION)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_CAPTION)).thenReturn(mockCaption);

        when(mediaJSON.has(Constants.JSON_COPYRIGHT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_COPYRIGHT)).thenReturn(mockCopyright);

        MediaEntity mediaEntity = new MediaEntity(mediaJSON);

        assertThat(mediaEntity.getUrl(),is(equalTo(mockUrl)));
        assertThat(mediaEntity.getFormat(),is(equalTo(mockFormatJSON)));
        assertThat(mediaEntity.getHeight(),is(mockHeight));
        assertThat(mediaEntity.getWidth(),is(mockWidth));
        assertThat(mediaEntity.getType(),is(equalTo(mockType)));
        assertThat(mediaEntity.getSubType(),is(equalTo(mockSubType)));
        assertThat(mediaEntity.getCaption(),is(equalTo(mockCaption)));
        assertThat(mediaEntity.getCopyright(),is(equalTo(mockCopyright)));
    }

    @Test
    public void testMediaEntity_MissingKeyAndValue() throws Exception {
        JSONObject mediaJSON =  mock(JSONObject.class);

        //Missing URL
        when(mediaJSON.has(Constants.JSON_MEDIA_URL)).thenReturn(false);

        when(mediaJSON.has(Constants.JSON_FORMAT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_FORMAT)).thenReturn(mockFormatJSON);

        //Missing URL
        when(mediaJSON.has(Constants.JSON_HEIGHT)).thenReturn(false);

        when(mediaJSON.has(Constants.JSON_WIDTH)).thenReturn(true);
        when(mediaJSON.getInt(Constants.JSON_WIDTH)).thenReturn(mockWidth);

        when(mediaJSON.has(Constants.JSON_TYPE)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_TYPE)).thenReturn(mockType);

        //Missing SUBTYPE
        when(mediaJSON.has(Constants.JSON_SUBTYPE)).thenReturn(false);

        when(mediaJSON.has(Constants.JSON_CAPTION)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_CAPTION)).thenReturn(mockCaption);

        when(mediaJSON.has(Constants.JSON_COPYRIGHT)).thenReturn(true);
        when(mediaJSON.getString(Constants.JSON_COPYRIGHT)).thenReturn(mockCopyright);

        MediaEntity mediaEntity = new MediaEntity(mediaJSON);

        assertThat(mediaEntity.getUrl(),is(equalTo(null)));
        assertThat(mediaEntity.getFormat(),is(equalTo(mockFormatJSON)));
        assertThat(mediaEntity.getHeight(),is(0));
        assertThat(mediaEntity.getWidth(),is(mockWidth));
        assertThat(mediaEntity.getType(),is(equalTo(mockType)));
        assertThat(mediaEntity.getSubType(),is(equalTo(null)));
        assertThat(mediaEntity.getCaption(),is(equalTo(mockCaption)));
        assertThat(mediaEntity.getCopyright(),is(equalTo(mockCopyright)));
    }


}

