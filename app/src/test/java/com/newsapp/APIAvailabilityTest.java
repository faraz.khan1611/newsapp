package com.newsapp;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import com.newsapp.Helper.Constants;

public class APIAvailabilityTest {

    //NOTE:: This case not be required as we always assume api is present while writing business logic,
    // instead we need to mock success and failure case for network call and check how app respond,
    // no infrastructure should be used to write unit test
    //LEAVING IT HERE AS IT WAS PART OF ASSIGNMENT
    @SuppressWarnings("MismatchedQueryAndUpdateOfStringBuilder")
    @Test public void testAvailability() throws Exception {
        URLConnection connection = new URL(Constants.NEWS_URL).openConnection();
        InputStream response = connection.getInputStream();

        StringBuilder buffer = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(response, Charset.defaultCharset()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                buffer.append(line);
            }
        }
        assert buffer.length() > 0;
    }
}
