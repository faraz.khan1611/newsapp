package com.newsapp.view.NewsList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import com.newsapp.Helper.Constants;
import com.newsapp.ModelLayer.Converter.ConverterImpl;
import com.newsapp.ModelLayer.Converter.IConverter;
import com.newsapp.ModelLayer.Entity.NewsEntity;
import com.newsapp.ModelLayer.InteractorLayer;
import com.newsapp.ModelLayer.InteractorLayerImpl;
import com.newsapp.ModelLayer.Network.Error;
import com.newsapp.ModelLayer.Network.INetworkCallbackInterface;
import com.newsapp.ModelLayer.Network.INetworkLayer;
import com.newsapp.view.NewsList.ICallback;
import com.newsapp.view.NewsList.NewsListPresenterImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewsListPresenterImplTest {


    @Captor
    private ArgumentCaptor<INetworkCallbackInterface<JSONObject>> callbackCaptor;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();


    @SuppressWarnings("unchecked")
    @Test
    public void VerifyLoadNewsResourceInPresenterForSuccess() throws JSONException {
        INetworkLayer networkLayer = mock(INetworkLayer.class);
        IConverter translationLayer = mock(ConverterImpl.class);
        ICallback<List<NewsEntity>> callback = mock(ICallback.class);
        InteractorLayer interactorLayer = new InteractorLayerImpl(networkLayer,translationLayer);

        NewsListPresenterImpl presenter = spy(new NewsListPresenterImpl(interactorLayer));
        presenter.setCallback(callback);

        doNothing().when(networkLayer).fetchDataWithURL(anyString(),callbackCaptor.capture());
        doCallRealMethod().when(translationLayer).convertToNewsEntityList(any(JSONObject.class));
        JSONObject mockObject = prepareMockResultJSONArrayToConvert();

        presenter.loadNewsResource();
        callbackCaptor.getValue().onSuccess(mockObject);

        verify(presenter,times(1)).accept(Mockito.<List<NewsEntity>>any());
        verify(callback,times(1)).onResult(Mockito.<List<NewsEntity>>any());
    }

    //BELOW CASE VERIFY SCENARIO WHEN CONFIGURATION IS CHANGED AND CALLBACK IS REMOVED
    @SuppressWarnings("unchecked")
    @Test
    public void VerifyLoadNewsResourceInPresenterForSuccessWithoutCallback() throws JSONException {
        INetworkLayer networkLayer = mock(INetworkLayer.class);
        IConverter translationLayer = mock(ConverterImpl.class);
        ICallback<List<NewsEntity>> callback = mock(ICallback.class);
        InteractorLayer interactorLayer = new InteractorLayerImpl(networkLayer,translationLayer);

        NewsListPresenterImpl presenter = spy(new NewsListPresenterImpl(interactorLayer));
        presenter.setCallback(callback);//SETTING CALLBACK HERE

        doNothing().when(networkLayer).fetchDataWithURL(anyString(),callbackCaptor.capture());
        doCallRealMethod().when(translationLayer).convertToNewsEntityList(any(JSONObject.class));
        JSONObject mockObject = prepareMockResultJSONArrayToConvert();

        presenter.loadNewsResource();
        presenter.removeCallback(); //REMOVED CALLBACK
        callbackCaptor.getValue().onSuccess(mockObject);

        verify(presenter,times(1)).accept(Mockito.<List<NewsEntity>>any());
        verify(callback,times(0)).onResult(Mockito.<List<NewsEntity>>any()); // CALLBACK SHOULD NOT BE CALLED HERE
    }

    @SuppressWarnings("unchecked")
    @Test
    public void VerifyLoadNewsResourceInPresenterForError() {
        INetworkLayer networkLayer = mock(INetworkLayer.class);
        IConverter translationLayer = mock(ConverterImpl.class);
        ICallback<List<NewsEntity>> callback = mock(ICallback.class);
        InteractorLayer interactorLayer = new InteractorLayerImpl(networkLayer,translationLayer);

        NewsListPresenterImpl presenter = spy(new NewsListPresenterImpl(interactorLayer));
        presenter.setCallback(callback);

        doNothing().when(networkLayer).fetchDataWithURL(anyString(),callbackCaptor.capture());

        presenter.loadNewsResource();
        callbackCaptor.getValue().onError(new Error("Test error"));
        verify(presenter,times(1)).accept(any(Error.class));
        verify(callback,times(1)).onError(any(Error.class));
    }

    private JSONObject prepareMockResultJSONArrayToConvert() throws JSONException {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        //region result array
        JSONArray resultJSONArrayMock = mock(JSONArray.class);
        JSONObject newsEntityJson = mock(JSONObject.class);

        //region newsEntityJSON
        when(newsEntityJson.has(Constants.JSON_TITLE)).thenReturn(true);
        String mockTitle = "Work Policies May Be Kinder, but Brutal Competition Isn’t";
        when(newsEntityJson.getString(Constants.JSON_TITLE)).thenReturn(mockTitle);

        when(newsEntityJson.has(Constants.JSON_ABSTRACT)).thenReturn(true);
        String mockSummary = "Top-tier employers may be changing their official policies in a nod to work-life balance";
        when(newsEntityJson.getString(Constants.JSON_ABSTRACT)).thenReturn(mockSummary);

        when(newsEntityJson.has(Constants.JSON_ARTICLE_URL)).thenReturn(true);
        String mockArticleUrl = "http://www.nytimes.com/2015/08/18/business/work";
        when(newsEntityJson.getString(Constants.JSON_ARTICLE_URL)).thenReturn(mockArticleUrl);

        when(newsEntityJson.has(Constants.JSON_BYLINE)).thenReturn(true);
        String mockByline = "by dev";
        when(newsEntityJson.getString(Constants.JSON_BYLINE)).thenReturn(mockByline);

        when(newsEntityJson.has(Constants.JSON_PUBLISHED_DATE)).thenReturn(true);
        String mockPublishedDate = "2015-08-17T22:10:02-5:00";
        when(newsEntityJson.getString(Constants.JSON_PUBLISHED_DATE)).thenReturn(mockPublishedDate);

        //region multimedia array
        JSONArray mockMultimediaJSONArray = mock(JSONArray.class);
        JSONObject mediaJSON = mock(JSONObject.class);

        when(mediaJSON.has(Constants.JSON_MEDIA_URL)).thenReturn(true);
        String mockUrl = "http://static01.nyt.com/images/2015/08/18/business/18EMPLOY/18EMPLOY-thumbStandard.jpg";
        when(mediaJSON.getString(Constants.JSON_MEDIA_URL)).thenReturn(mockUrl);

        when(mediaJSON.has(Constants.JSON_FORMAT)).thenReturn(true);
        String formatJSON = "Standard Thumbnail";
        when(mediaJSON.getString(Constants.JSON_FORMAT)).thenReturn(formatJSON);

        when(mediaJSON.has(Constants.JSON_HEIGHT)).thenReturn(true);
        int mockHeight = 75;
        when(mediaJSON.getInt(Constants.JSON_HEIGHT)).thenReturn(mockHeight);

        when(mediaJSON.has(Constants.JSON_WIDTH)).thenReturn(true);
        int mockWidth = 75;
        when(mediaJSON.getInt(Constants.JSON_WIDTH)).thenReturn(mockWidth);

        when(mediaJSON.has(Constants.JSON_TYPE)).thenReturn(true);
        String mockType = "image";
        when(mediaJSON.getString(Constants.JSON_TYPE)).thenReturn(mockType);

        when(mediaJSON.has(Constants.JSON_SUBTYPE)).thenReturn(true);
        String mockSubType = "photo";
        when(mediaJSON.getString(Constants.JSON_SUBTYPE)).thenReturn(mockSubType);

        when(mediaJSON.has(Constants.JSON_CAPTION)).thenReturn(true);
        String mockCaption = "People eating at the Brave Horse Tavern on the Amazon campus in Seattle in June.";
        when(mediaJSON.getString(Constants.JSON_CAPTION)).thenReturn(mockCaption);

        when(mediaJSON.has(Constants.JSON_COPYRIGHT)).thenReturn(true);
        String mockCopyright = "Matthew Ryan Williams for The New York Times";
        when(mediaJSON.getString(Constants.JSON_COPYRIGHT)).thenReturn(mockCopyright);

        doReturn(1)
                .when(mockMultimediaJSONArray).length();

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).get(0);

        doReturn(mediaJSON)
                .when(mockMultimediaJSONArray).getJSONObject(0);

        when(newsEntityJson.has(Constants.JSON_MULTIMEDIA)).thenReturn(true);
        when(newsEntityJson.get(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        when(newsEntityJson.getJSONArray(Constants.JSON_MULTIMEDIA)).thenReturn(mockMultimediaJSONArray);
        //endregion


        doReturn(1)
                .when(resultJSONArrayMock).length();

        doReturn(newsEntityJson)
                .when(resultJSONArrayMock).get(0);

        doReturn(newsEntityJson)
                .when(resultJSONArrayMock).getJSONObject(0);
        //endregion

        when(jsonObjectMock.has(Constants.JSON_RESULTS)).thenReturn(true);
        when(jsonObjectMock.getJSONArray(Constants.JSON_RESULTS)).thenReturn(resultJSONArrayMock);
        //endregion

        return jsonObjectMock;

    }
}
